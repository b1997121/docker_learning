# \# IMAGE
[More image information](https://docs.docker.com/engine/reference/commandline/image/)

[More images information](https://docs.docker.com/engine/reference/commandline/images/)

## Download image
```
docker pull ubuntu:21.04
```

## List images
```
docker image ls
docker images
```

## Remove image
```
docker rmi images
```

# \# CONTAINER 
[More container information](https://docs.docker.com/engine/reference/commandline/container/)

## Create && run container
```
docker run -it ubuntu
docker run -it [IMAGE ID/REPOSITORY:TAG]

Option
--name [NAMES]: assign a name for this container
```

## Stop a container
```
docker stop [CONTAINER ID/NAMES]
```

## Start a stop container
```
docker start -ai [CONTAINER ID/NAMES]
```

## List container
```
docker ps
docker ps -a
docker container ls -a
```

## Remove container
```
docker rm [CONTAINER ID/NAMES]
docker container rm [CONTAINER ID/NAMES]
```

## From container back to original OS
```
Ctrl + p && Ctrl + q
```

## Back to container
```
docker attach [CONTAINER ID/NAMES]
docker exec -it [CONTAINER ID/NAMES] [COMMAND]
nsenter --target $(docker inspect --format "{{ .State.Pid }}" <container>) --mount --uts --ipc --net --pid  # Use exit command only logout not stop container, not support Ctrl + p && Ctrl + q
```

# \# LOCAL REGISTRY

## Start local registry
```
docker run -d -p 5000:5000 --restart=always --name registry -v /mnt/registry:/var/lib/registry registry:2
```
[More run information](https://docs.docker.com/engine/reference/commandline/run/)

## Copy and change image tag (if want push to local registry must change REPOSITORY to localhost:5000)
```
docker tag ubuntu/update:21.04 localhost:5000/u21.04-update:21.04
```

## Push images into local regitstry
```
docker push localhost:5000/u21.04-update:21.04
```

# \# VOLUME
[More volume information 2](https://docs.docker.com/engine/reference/commandline/volume_create/)

[More volume information 2](https://docs.docker.com/storage/volumes/)

## Create volume
```
docker volume create volume_test  # Wil create a volume in root-data path
docker volume create --opt type=ext4 --opt device=/dev/sdb volume_test2 # The sdb must exist in local OS
docker volume create --opt type=none --opt device=/folder/path test3 --opt o=bind # Create volume and bind in /folder/path
docker volume create --driver vieux/sshfs -o sshcmd=root@192.148.255.242:/root/Documents/Xin -o password=123456 sshvolume # Create remote volume, must install plugin "vieux/sshfs"
```

## List volume
```
docker volume ls
```

## Check more volume information
```
docker volume inspect [VOLUME NAME]
``` 

## Remove volume
```
docker volume rm [VOLUME NAME]
```

## Using volume
```
docker run -it -v volume_test2:/mnt/ --name vt2 ubuntu:21.04
```

## Mount OS current folder as volume
```
docker run -it -v /root/docker_mount:/tmp centos:7 --name vt3
```

# \# Add a Volume to an Existing(running) Docker Container
## Stop the container
``` sh
$ docker stop daea39c56d8b
daea39c56d8b
```

## Commit the stopped container
``` sh
$ docker commit daea39c56d8b temp/compiler
sha256:b93ddd768c6dfb57ecacd0aed3472f0c2a438d797ac28a287e6d9e9242e4f52e
```

## Run with the commited image with volume
``` sh
$ docker run -it -v for_compiler:/homw/compiler_folder --name compiler temp/compiler
```

# \# PUSH Docker image to Docker hub
## Create a account from Docker hub
[Docker hub](https://hub.docker.com/)

## Create a tag that refers to the specific image
``` sh
$ docker images
REPOSITORY   TAG       IMAGE ID       CREATED         SIZE
compiler     1.0       9337945809f3   6 minutes ago   408MB

$ docker tag compiler:1.0 b199712/compiler:1.0

$ docker images
REPOSITORY         TAG       IMAGE ID       CREATED          SIZE
compiler           1.0       9337945809f3   15 minutes ago   408MB
b199712/compiler   1.0       9337945809f3   15 minutes ago   408MB
```

## Push image to the Docker hub
``` sh
$ docker push b199712/compiler:1.0
The push refers to repository [docker.io/b199712/compiler]
75feca344749: Layer already exists
3f45d53e122b: Layer already exists
1bdbfdd3866b: Layer already exists
6b6e6a964497: Layer already exists
2138ce42bd19: Layer already exists
4bdb65d9d7ad: Layer already exists
e15f3a9ea51d: Layer already exists
20dceb1d36c0: Layer already exists
d56ee7bd6c79: Layer already exists
74c23523118f: Layer already exists
1251204ef8fc: Layer already exists
47ef83afae74: Layer already exists
df54c846128d: Layer already exists
be96a3f634de: Layer already exists
1.0: digest: sha256:e323ec92e93964c382faed8886945c5e208a613f25085bda852cb5054a9e3b68 size: 3263

```

# \# NETWORK
## Create "macvlan" network
```sh
$ docker network create -d macvlan --subnet=192.168.118.128/25 --gateway=192.168.118.254 -o parent=ens65f1np1 test-net
075997f267cb0226a0b2d23f954358884c126c9d8a7d493c7aa02ca497d26c3d
```

## List network
```sh
$ docker network ls
NETWORK ID     NAME       DRIVER    SCOPE
95aee4d28f48   bridge     bridge    local
de57b4b83c02   host       host      local
a977dd3cde44   none       null      local
075997f267cb   test-net   macvlan   local
```


## Use network
```sh
$ docker run -it --net=test-net --name git-runner ubuntu:latest
```

# \# Gitlab Runner
## pull gitlab-runner
```sh
$ docker pull gitlab/gitlab-runner
Using default tag: latest
latest: Pulling from gitlab/gitlab-runner
675920708c8b: Pull complete
1ba641ade6d5: Pull complete
0a7bc84afd51: Pull complete
Digest: sha256:ca9b2e681a5ef18c594a13ae28c18f993a44a88c35aa1d10a3f544cbda8c69ea
Status: Downloaded newer image for gitlab/gitlab-runner:latest
docker.io/gitlab/gitlab-runner:latest
```

## create a volume for gitlab-runner
```sh
$ docker volume create --opt type=none --opt device=/mnt/git_runner_folder for_git-runner --opt o=bind
for_git-runner
```

## run gitlab-runner with volume in bg
```sh
$ docker run -d --name gitlab-runner --restart always -v /var/run/docker.sock:/var/run/docker.sock -v for_git-runner:/etc/gitlab-runner gitlab/gitlab-runner:latest
b3fc044ebe25f713a60f06b89684e3e3f6230b7ff2ded7360282e45c136ae40f
```

## regiseter runner
```sh
$ docker run --rm -it -v for_git-runner:/etc/gitlab-runner gitlab/gitlab-runner:latest register
Runtime platform                                    arch=amd64 os=linux pid=7 revision=43b2dc3d version=15.4.0
Running in system-mode.

Enter the GitLab instance URL (for example, https://gitlab.com/):
[URL from gitlab]
Enter the registration token:
[Token for gitlab]
Enter a description for the runner:
[9d6b03b089c0]: brian
Enter tags for the runner (comma-separated):
test
Enter optional maintenance note for the runner:
for test
Registering runner... succeeded                     runner=knMCrFCa
Enter an executor: docker, docker-ssh, ssh, custom, shell, virtualbox, docker+machine, docker-ssh+machine, kubernetes, parallels:
shell
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!

Configuration (with the authentication token) was saved in "/etc/gitlab-runner/config.toml"

```

## list runner
```sh
$ docker run --rm -it -v for_git-runner:/etc/gitlab-runner gitlab/gitlab-runner:latest list
Runtime platform                                    arch=amd64 os=linux pid=7 revision=43b2dc3d version=15.4.0
Listing configured runners                          ConfigFile=/etc/gitlab-runner/config.toml
brian                                               Executor=shell Token=[Token for gitlab] URL=[URL from gitlab]
```

# \# Export/Import image

## export
```sh
docker save -o compiler.tar compiler
```

### import
```sh
docker load < compiler.tar
```

# \# Change docker service default directory
[Refer URL](https://docs.expertflow.com/chat/3.14/troubleshooting-and-maintenance/change-docker-default-directory)

1. Create or edit file /etc/docker/daemon.json
2. Add below content to the file
```json
{
  "data-root": "/home/var"  
}
```
3. Restart docker service
```sh
systemctl restart docker
```

# \# Start Container with IPv6

To enable IPv6 in Docker, first create the file /etc/docker/daemon.json with the following content (or if it already exists, add the values):

[Refer URL](https://medium.com/@skleeschulte/how-to-enable-ipv6-for-docker-containers-on-ubuntu-18-04-c68394a219a2)

```json
{
  "ipv6": true,
  "fixed-cidr-v6": "fd00::/80"
}
```

Then restart Docker:
```bash
systemctl restart docker
```

Configuring IPv6 NAT
```bash
ip6tables -t nat -A POSTROUTING -s fd00::/80 ! -o docker0 -j MASQUERADE
```
